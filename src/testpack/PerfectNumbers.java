package testpack;

//Java Program to Find Perfect Number between 33,550,300-33,550,400
import java.util.Scanner;

public class PerfectNumbers {
	private static Scanner sc;

	public static void main(String[] args) {
		int i, Number, lowernum, uppernum, Sum = 0;
		sc = new Scanner(System.in);

		System.out.println("Please Enter the lowernumber Value: ");// enter
																	// 33,550,300
		lowernum = sc.nextInt();

		System.out.println("Please Enter the uppernumber Value: ");// enter
																	// 33,550,400
		uppernum = sc.nextInt();

		for (Number = lowernum; Number <= uppernum; Number++) {
			for (i = 1, Sum = 0; i < Number; i++) {
				if (Number % i == 0) {
					Sum = Sum + i;
				}
			}
			if (Sum == Number) {
				System.out.format("%d \t", Number);
			}
		}
	}
}

// Question : Are there any perfect numbers between 33,550,300 and 33,550,400?
// Answer : yes 33550336 is the perfect number between 33,550,300 and
// 33,550,400.